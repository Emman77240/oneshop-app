module.exports.routes = {
  '/logout': 'UserController.logout',
  'POST /login': 'UserController.login',
  'POST /api/user': 'UserController.signup',
  'GET /api/order/:userId': 'OrderController.fetch',
  'POST /api/order/:userId': 'OrderController.create',
 };
