module.exports = {

  connection: 'mongo_db',
  schema: true,
  attributes: {
    order: {
      type: 'string'
    },
    userId: {
      model: 'user'
    }
  }
};


