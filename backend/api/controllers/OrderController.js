require('dotenv').config();

const sgMail = require('@sendgrid/mail');
sgMail.setApiKey(process.env.SG_KEY);

module.exports = {

  create: function(req, res) {
    var order = req.body.order;
    var userId = req.params.userId;
    var message = "You created a new order. Thanks for your purchase!"

    User.findOne({ id: userId }).exec(function(error, user) {
      if (error) {
        return res.json(500, { status: false, message: "Order could not be created" });
      }
      if(!user){
        return res.json({status: false, message: "User does not exist", user: {}})
      }

      Order.create({ order: order, userId: userId }).exec(function(error, newOrder) {
        if (error) {
          return res.json(500, { status: "Order could not be created" });
        }
        const msg = {
              to: user.email,
              from: process.env.OneShop_Mail,
              subject: 'New Order alert',
              text: 'You have placed a new order',
              html: '<html><body><p>'+ message +'</p></body></html>'
             };
             sgMail.send(msg);

        return res.json({status: true, message: "", order: newOrder });
      });
    
    });

  },

  fetch: function(req, res) {
    var userId = req.params.userId;

    Order.find({ userId: userId }).exec(function(error, orders) {
      if (error) {
        return res.json(500, { status:false, message: "Could not fetch orders" });
      }
      return res.json({status: true, message: "", orders: orders });
    });
  },

};



