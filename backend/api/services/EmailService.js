require('dotenv').config();

const sgMail = require('@sendgrid/mail');
sgMail.setApiKey(process.env.SG_KEY);


module.exports = {
  sendEmail: function(message, email) {
    const msg = {
              to: email,
              from: process.env.OneShop_Mail,
              subject: 'New Order alert',
              text: 'You have placed a new order',
              html: '<html><body><p>'+ message +'</p></body></html>'
             };
             sgMail.send(msg);
    
  }
};
