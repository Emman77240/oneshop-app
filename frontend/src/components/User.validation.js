import isEmpty from 'lodash/isEmpty'
import validate from 'validate.js'

export default {
  data: () => ({
    errors: {
      email: [],
      password: []
    }
  }),

  computed: {
    isValidEmail: {
      get () {
        if (isEmpty(this.user.email)) return null

        const validation = validate.single(this.user.email, {
          presence: true,
          length: {
            minimum: 8,
            message: 'Email must be at least eight characters long'
          }
        })

        if (validation) {
          this.errors.email = validation

          return false
        }

        return true
      }
    },

    isValidPassword: {
      get () {
        if (isEmpty(this.user.password)) return null

        const validation = validate.single(this.user.password, {
          presence: true,
          length: {
            minimum: 3,
            message: 'Password must be at least three characters long'
          }
        })

        if (validation) {
          this.errors.password = validation

          return false
        }

        return true
      }
    }
  }
}
