import Vue from 'vue'
import Axios from 'axios'
import Router from 'vue-router'
import VueResource from 'vue-resource'
import VueSession from 'vue-session'
import Welcome from '@/components/Welcome'
import Login from '@/components/Login'
import Signup from '@/components/Signup'
import Dashboard from '@/components/Dashboard'
import Order from '@/components/Order'

Vue.use(Router);
Vue.use(VueSession);
Vue.use(VueResource);

export default new Router({
  routes: [
    { path: '', name: 'Welcome', component: Welcome },
    { path: '/login', name: 'Login', component: Login},
    { path: '/signup', name: 'Signup', component: Signup },
    { path: '/dashboard', name: 'Dashboard', component: Dashboard },
    { path: '/order', name: 'Order', component: Order },
    {path: '*', redirect: '/'},
  ]
})
