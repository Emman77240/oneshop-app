import Vue from 'vue';
import Vuex from 'vuex';

Vue.use(Vuex);

export default new Vuex.Store({
  state: {
    forSale: [
      { invId: 1, name: 'iPhone X', image: "http://mac-more.com/image/data/iPhone/iPhone%20X/0008671_apple-iphone-x-a1901-256gb-space-grey.jpeg", price: 99900 },
      { invId: 2, name: 'Mac book pro S5', image: "https://www.portableone.com/resize/Shared/Images/Product/Apple-MacBook-Pro-13-Retina-with-Touch-Bar-Z0V9-Silver-Customize-Mid-2018/MBP13-Touch-Silver.jpg?bw=500&bh=500", price: 179999 },
      { invId: 3, name: 'Asus Gamers', image: "https://img-us1.asus.com/A/show/AW000706/2018/0831/AM0000003/201808AM310000003_15356734053373050023670.jpg!t500x500", price: 189999 },
      { invId: 4, name: 'iPhone XS', image: "https://i.expansys.net/img/p/312956/apple-iphone-xs-a1920.jpg", price: 134900 },
    ],
    inCart: [],
  },
  getters: {
    forSale: state => state.forSale,
    inCart: state => state.inCart,
  },
  mutations: {
    ADD_TO_CART(state, invId) { state.inCart.push(invId); },
    REMOVE_FROM_CART(state, index) { state.inCart.splice(index, 1); }
  },
  actions: {
    addToCart(context, invId) { context.commit('ADD_TO_CART', invId) },
    removeFromCart(context, index) { context.commit('REMOVE_FROM_CART', index); },
  },
});