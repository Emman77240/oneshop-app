# OneShop app

This project is for those who are new to single-page applications and want to learn through a real example. Besides that, it should cover most of the features from Sails.js and Vue.js, like a reference book. For a better understanding, you should be aware of JavaScript ES6 features and also async/await.

To see this project in action, visit https://oneshopapp.herokuapp.com

## Prerequisites
To get started, you need Node.js. It's also recommend to have Sails.js globally installed.

## Get Node.js
$ curl -sL https://deb.nodesource.com/setup_10.x | sudo -E bash -
$ sudo apt-get install -y nodejs

## Get Sails.js (optional)
$ sudo npm install sails -g

## Install modules
$ cd frontend && npm install
$ cd ../backend && npm install

## Usage

## Development
cd backend && sails lift and then cd ../frontend && npm run dev. After that, open localhost:8080 in your browser. Make sure that you start both servers simultaneously.

## Production
First, you have to build up your Vue.js components and merge them with Sails.js. This can be done with cd frontend && npm run build. Now do cd ../backend && NODE_ENV=production node app.js and then open your browser and go to localhost:1337.

## Essential components used
The following components are used in this project. There are plenty more, though, check the package.json files.

## Sails.js
This is the backend and data provider.

## Vue.js
Handle frontend data with a MVVM.

## Vuex
A state pattern.

## BootstrapVue
Frontend framework. The design part.

## Vue-resource
HTTP client for Vue.js.

## Vue-router
Router for the frontend.

## Code style
This project fulfils the JavaScript Standard Style.


